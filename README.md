# Prototype for a WKHTMLTOPDF replacement

Only used for experimentation so far.

## Usage via node

1. Run `start:dev` script
2. Checkout nice2 branch `features/wkhtmltopdf-replacement` and look for the string `https://report-service.tocco.ch` inside the project and replace it with `https://localhost:3000`.
3. Run nice locally and create a custom create a new custom report for example for `Person` entity. From the `Person` list, you can generate a new report.

## Usage via Docker

1. Start your local Docker environment
2. Build the container: `docker build -t tocco-report-engine .`
3. Openshift runs the container with a random user, so to be as close as possible to the production setup, run the Docker using this command:
   - `docker run -u 12345678:0 -p 3000:3000 tocco-report-engine`

## Environment variables

System can be configured with `.env` file through `dotenv`.

### Settings

- `FILE_RETENTION`
  - value `KEEP` retains input and output files after generation
  - any other value deletes all files after generation
- `API_KEY`
- `PROFILING`
  - value `ENABLED` enables request profiler
  - any other value disables the request profiler
- `CONCURRENCY_AMOUNT`
  - how many concurrent puppeteer requests should be used
  - default 10
- `PRODUCTION`
  - any value set enables production mode

### Example `.env` file

```
FILE_RETENTION=KEEP
API_KEY=XXX-XXX-XXX-XXX
PROFILING=ENABLED
CONCURRENCY_AMOUNT=10
PRODUCTION=true
```
