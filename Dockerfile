FROM node:20.15.0-slim

# Prepare the container to run headless chrome
# There is also a pre-built puppeteer docker image, but I decided to include this in our
# Dockerfile because that way we have all the versions under control ourselves.
# See https://pptr.dev/troubleshooting#running-puppeteer-in-docker for more info
RUN apt-get update \
    && apt-get install -y wget gnupg \
    && wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
    && sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list' \
    && apt-get update \
    && apt-get install -y google-chrome-stable fonts-ipafont-gothic fonts-wqy-zenhei fonts-thai-tlwg fonts-kacst fonts-freefont-ttf libxss1 \
      --no-install-recommends \
    && rm -rf /var/lib/apt/lists/*

# OpenShift compatability
# https://docs.tocco.ch/devops/service_setup.html#prepare-a-docker-image
# https://docs.openshift.com/container-platform/4.16/openshift_images/create-images.html#images-create-guide-openshift_create-images
RUN chgrp -R 0 /home/node \
    && chmod -R g+rw /home/node

WORKDIR /usr/src/app

# Add required project resources
COPY package.json ./
COPY package-lock.json ./
COPY tsconfig.json ./
COPY src ./src

# Create and own tmp folder
RUN mkdir tmp && chown node:0 tmp && chmod 770 tmp

# Tell puppeteer to not download a browser and install project dependencies
ENV PUPPETEER_SKIP_DOWNLOAD=true
RUN npm i

# Compile TypeScript into JS and remove original sources inside container
RUN npm run build
RUN rm -rf src

# OpenShift runs job as an arbitrary user without name or home folder.
# Thus, set those here explicitly.
ENV USER=node
ENV HOME=/home/node

# Let our own code know that it is running in production
ENV PRODUCTION=true

EXPOSE 3000
CMD ["npm", "run", "start:prod"]
