import {getHtmlFilePath, getFileExtension, deleteFolder} from './files'
import fs from 'fs'

jest.mock('fs')

describe('files', () => {
  describe('getHtmlFilePath', () => {
    test('should join folder and html path', () => {
      const result = getHtmlFilePath('folder', [
        {data: Buffer.alloc(0), mode: 0, path: 'index.html', mtime: '', type: ''}
      ])
      expect(result).toBe('folder/index.html')
    })
  })

  describe('getFileExtension', () => {
    test('should find extension', () => {
      expect(getFileExtension('index.html')).toBe('html')
    })

    test('should return empty string on no extension', () => {
      expect(getFileExtension('file')).toBe('')
    })
  })

  describe('deleteFolder', () => {
    test('should delete folder recursively', () => {
      deleteFolder('folder/path')
      expect(fs.rmSync).toHaveBeenCalledWith('folder/path', {force: true, recursive: true})
    })
  })
})
