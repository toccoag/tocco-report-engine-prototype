import {RequestHandler} from 'express'

export const requestProfiler = (): RequestHandler => {
  if (process.env.PROFILING === 'ENABLED') {
    return (req, _res, next) => {
      const identifier = `${new Date().toLocaleString()} ${req.method} ${req.path}`
      console.time(identifier)
      next()
      console.timeEnd(identifier)
    }
  } else {
    return (_req, _res, next) => next()
  }
}
