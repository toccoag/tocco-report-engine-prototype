import {Page} from 'puppeteer'
import {pageToPdf} from './generation'
import fs from 'fs'

describe('generation', () => {
  describe('pageToPdf', () => {
    const getMockedPage = (): Page =>
      ({
        on: jest.fn(),
        goto: jest.fn(),
        waitForSelector: jest.fn(),
        pdf: jest.fn(),
        close: jest.fn()
      }) as unknown as Page

    test('should call puppeteer methods', async () => {
      fs.existsSync = jest.fn().mockReturnValue(true)

      const expectedOutputPath = 'folder/output.pdf'
      const page = getMockedPage()

      const result = await pageToPdf({page, data: 'folder/file.html', worker: {id: 0}})
      expect(result).toBe(expectedOutputPath)

      expect(page.on).toHaveBeenCalledWith('dialog', expect.any(Function))
      expect(page.goto).toHaveBeenCalledWith(`file://folder/file.html`, {waitUntil: 'domcontentloaded'})
      expect(page.waitForSelector).toHaveBeenCalledWith('.pagedjs_pages', {visible: true})
      expect(page.pdf).toHaveBeenCalledWith({
        path: expectedOutputPath,
        preferCSSPageSize: true,
        printBackground: true,
        margin: {top: 0, bottom: 0, left: 0, right: 0}
      })
      expect(page.close).toHaveBeenCalled()
    })
  })
})
