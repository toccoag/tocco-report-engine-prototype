import {TaskFunction} from 'puppeteer-cluster/dist/Cluster'

import fs from 'fs'
import path from 'path'

export const pageToPdf: TaskFunction<string, string> = async ({page, data: htmlPath}) => {
  // TODO how do we deal with dialogs correctly?
  page.on('dialog', async dialog => {
    await dialog.accept()
  })

  const outputPath = path.join(path.dirname(htmlPath), 'output.pdf')
  if (!fs.existsSync(htmlPath)) {
    throw Error('Input files missing')
  }
  await page.goto(`file://${htmlPath}`, {waitUntil: 'domcontentloaded'})
  await page.waitForSelector('.pagedjs_pages', {visible: true})

  await page.pdf({
    path: outputPath,
    preferCSSPageSize: true,
    printBackground: true,
    margin: {top: 0, bottom: 0, left: 0, right: 0}
  })

  await page.close()

  return outputPath
}
