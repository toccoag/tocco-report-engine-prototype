import {Request, Response} from 'express'

import os from 'os'
import path from 'path'
import decompress from 'decompress'
import crypto from 'crypto'
import {Cluster} from 'puppeteer-cluster'

import {getFileExtension, deleteFolder, getHtmlFilePath} from './files'

interface GenerationResult {
  success: boolean
  errorMessage: string | null
}

const handleError = (error: unknown, res: Response, req: Request, targetFolder: string) => {
  console.error(error)

  if (req.file) {
    deleteFolder(req.file.path)
  }
  deleteFolder(targetFolder)

  const message = getErrorMessage(error, res)
  res.send(getErrorResult(message))
}

const getErrorMessage = (error: unknown, res: Response): string => {
  if (typeof error === 'string') {
    res.status(400)
    return error.toUpperCase()
  } else if (error instanceof Error) {
    res.status(400)
    return error.message
  } else {
    res.status(500)
    return 'Something went wrong'
  }
}

const getErrorResult = (message: string): GenerationResult => ({success: false, errorMessage: message})

export const handleUpload = (req: Request, res: Response, browserCluster: Cluster<string, string>) => {
  const authKey = req.header('Authorization')?.replace('Bearer ', '')
  if (authKey !== process.env.API_KEY) {
    res.status(403).send('Unauthorized')
    return
  }

  const file = req.file
  const targetFolder = path.join(os.tmpdir(), crypto.randomUUID())

  try {
    if (!file || getFileExtension(file.originalname) !== 'zip') {
      throw new Error('zip file must be uploaded!')
    }

    console.log(`uploaded file: ${file.path}`)
    console.log(`target folder: ${targetFolder}`)

    decompress(file.path, targetFolder)
      .then(files => getHtmlFilePath(targetFolder, files))
      .then(htmlFilePath => browserCluster.execute(htmlFilePath))
      .then(outputPath =>
        res.sendFile(outputPath, error => {
          if (error) {
            console.error('error sending file:', error)
          } else {
            if (process.env.FILE_RETENTION === 'KEEP') {
              console.log('keeping files')
            } else {
              deleteFolder(file.path)
              deleteFolder(targetFolder)
            }
          }
        })
      )
  } catch (error) {
    handleError(error, res, req, targetFolder)
  }
}
