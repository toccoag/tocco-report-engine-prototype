import express from 'express'
import multer from 'multer'

import os from 'os'
import {Cluster} from 'puppeteer-cluster'

import * as dotenv from 'dotenv'
import {getClusterOptions} from './options'
import {pageToPdf} from './generation'
import {handleUpload} from './upload'
import {requestProfiler} from './profiling'

dotenv.config()

let browserCluster: Cluster<string, string> | null = null
Cluster.launch(getClusterOptions()).then(async cluster => {
  await cluster.task(pageToPdf)
  console.log('Browser cluster started successfully')
  return (browserCluster = cluster)
})

const upload = multer({dest: os.tmpdir()})
const app = express()

app.post('/upload', requestProfiler(), upload.single('file'), (req, res) => {
  if (browserCluster == null) {
    throw Error('Browser cluster has not yet been initialized!')
  }
  handleUpload(req, res, browserCluster)
})
app.listen(3000, () => {
  console.log('The application is listening on localhost:3000!')
})
