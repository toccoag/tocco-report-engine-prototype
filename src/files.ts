import fs from 'fs'
import path from 'path'
import decompress from 'decompress'

export const getHtmlFilePath = (folderPath: string, files: decompress.File[]): string => {
  const htmlPath = files.map(file => file.path).find(p => getFileExtension(p) === 'html')
  if (htmlPath === undefined) {
    throw new Error('no html file found in uploaded zip')
  }
  return path.join(folderPath, htmlPath)
}

export const getFileExtension = (filePath: string) => {
  return path.extname(filePath).substring(1)
}

export const deleteFolder = (rmPath: string) => {
  console.log(`deleting ${rmPath}`)
  fs.rmSync(rmPath, {force: true, recursive: true})
}
