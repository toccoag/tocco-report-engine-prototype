import {Cluster} from 'puppeteer-cluster'

// List found in https://github.com/puppeteer/puppeteer/issues/10829
// exported for easy testing
export const DEFAULT_PUPPETEER_ARGS = [
  '--disable-features=IsolateOrigins',
  '--disable-site-isolation-trials',
  '--autoplay-policy=user-gesture-required',
  '--disable-background-networking',
  '--disable-background-timer-throttling',
  '--disable-backgrounding-occluded-windows',
  '--disable-breakpad',
  '--disable-client-side-phishing-detection',
  '--disable-component-update',
  '--disable-default-apps',
  '--disable-dev-shm-usage',
  '--disable-domain-reliability',
  '--disable-extensions',
  '--disable-features=AudioServiceOutOfProcess',
  '--disable-gpu',
  '--disable-hang-monitor',
  '--disable-ipc-flooding-protection',
  '--disable-notifications',
  '--disable-offer-store-unmasked-wallet-cards',
  '--disable-popup-blocking',
  '--disable-print-preview',
  '--disable-prompt-on-repost',
  '--disable-renderer-backgrounding',
  '--disable-setuid-sandbox',
  '--disable-speech-api',
  '--disable-sync',
  '--hide-scrollbars',
  '--ignore-gpu-blacklist',
  '--metrics-recording-only',
  '--mute-audio',
  '--no-default-browser-check',
  '--no-first-run',
  '--no-pings',
  '--no-sandbox',
  '--no-zygote',
  '--password-store=basic',
  '--use-gl=swiftshader',
  '--use-mock-keychain',
  '--headless'
]

const getPuppeteerLaunchOptions = () => {
  if (process.env.PRODUCTION) {
    console.log('Starting in PROD mode...')
    return {
      headless: true,
      args: DEFAULT_PUPPETEER_ARGS,
      userDataDir: './tmp',
      executablePath: '/usr/bin/google-chrome'
    }
  } else {
    console.log('Starting in DEV mode...')
    return {
      headless: true,
      args: DEFAULT_PUPPETEER_ARGS,
      userDataDir: './tmp',
      devtools: true
    }
  }
}

export const getClusterOptions = () => ({
  puppeteerOptions: getPuppeteerLaunchOptions(),
  concurrency: Cluster.CONCURRENCY_CONTEXT,
  maxConcurrency: parseInt(process.env.CONCURRENCY_AMOUNT || '10')
})
