import {getClusterOptions, DEFAULT_PUPPETEER_ARGS} from './options'
import {Cluster} from 'puppeteer-cluster'

describe('options', () => {
  describe('getClusterOptions', () => {
    test('should use production options', () => {
      process.env.PRODUCTION = 'true'
      const result = getClusterOptions()
      expect(result.puppeteerOptions.executablePath).toBe('/usr/bin/google-chrome')
      expect(result.puppeteerOptions.devtools).toBe(undefined)
      delete process.env.PRODUCTION
    })

    test('should use development options', () => {
      const result = getClusterOptions()
      expect(result.puppeteerOptions.executablePath).toBe(undefined)
      expect(result.puppeteerOptions.devtools).toBe(true)
    })

    test('should use environment concurrency', () => {
      process.env.CONCURRENCY_AMOUNT = '5'
      const result = getClusterOptions()
      expect(result.maxConcurrency).toBe(5)
      delete process.env.CONCURRENCY_AMOUNT
    })

    test('should use default options', () => {
      const result = getClusterOptions()
      expect(result.puppeteerOptions.args).toBe(DEFAULT_PUPPETEER_ARGS)
      expect(result.puppeteerOptions.headless).toBe(true)
      expect(result.puppeteerOptions.userDataDir).toBe('./tmp')
      expect(result.concurrency).toBe(Cluster.CONCURRENCY_CONTEXT)
      expect(result.maxConcurrency).toBe(10)
    })
  })
})
